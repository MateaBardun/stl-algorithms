#include <iostream>
#include <algorithm>
#include <vector>
#include <random>
#include <iterator>
#include <numeric>
#include <functional>
#include <set>
#include "STLAlgorithms.h"


int main()
{
	create_numbers();

	print_component("Heap operations");
	heap_operations();

	print_component("Sort operations");
	sort_operations();

	print_component("Partitioning operations");
	partitioning_operations();

	print_component("Queries");
	queries();

	print_component("Comparing");
	comparing();

	print_component("Searching");
	searching();

	print_component("Set algorithms");
	set_algorithms();

	print_component("Movers");
	movers();

	print_component("Value modifiers");
	value_modifiers();

	print_component("Structure changers");
	structure_changers();

	print_component("Other algorithms");
	others();

	cout << '\n';
}

void print_component(string text)
{
	cout << "\n\n\n" << text << "\n\n\n";
}

void create_numbers()
{
	for (int i = 0; i < 30; ++i)
	{
		numbers.push_back(rand() % 30 + 1);
	}
}
//shuffle
void shuffle_numbers()
{
	random_device rd;
	mt19937 g(rd());
	shuffle(numbers.begin(), numbers.end(), g);
}
//for_each
template<typename Container>
void print(const Container &numbers, const string &text)
{
	auto print_command = [](const int& n) { cout << n << " "; };
	cout << text;
	for_each(numbers.begin(), numbers.end(), print_command);
	cout << "\n";
}

//make_heap, pop_heap, push_heap, sort_heap
void heap_operations()
{
	print(numbers, "Numbers as a vector\t");

	make_heap(numbers.begin(), numbers.end());
	print(numbers, "Numbers as a heap\t");

	pop_heap(numbers.begin(), numbers.end());
	print(numbers, "Numbers after pop_heap\t");

	push_heap(numbers.begin(), numbers.end());
	print(numbers, "Numbers after push_heap\t");

	sort_heap(numbers.begin(), numbers.end());
	print(numbers, "Numbers after sort_heap\t");
}

//sort, partial_sort, inplace_merge
void sort_operations()
{
	shuffle_numbers();
	print(numbers, "Not sotred numbers:\t\t\t");
	
	auto middle = numbers.begin() + (numbers.end() - numbers.begin()) / 2;
	partial_sort(numbers.begin(), middle, numbers.end());
	print(numbers, "Numbers sorted to middle position:\t");

	sort(middle, numbers.end());
	inplace_merge(numbers.begin(), middle, numbers.end());
	print(numbers, "Numbers sorted with inplace_merge:\t");
}

//reverse, rotate, next_permutation, prev_permutation, partition, copy, partition_point
void partitioning_operations()
{
	reverse(numbers.begin(), numbers.end());
	print(numbers, "Reversed numbers: \t\t\t");

	rotate(numbers.begin(), numbers.begin() + 1, numbers.end());
	print(numbers, "Rotated numbers by one:\t\t\t");

	next_permutation(numbers.begin(), numbers.end());
	print(numbers, "Next permutation of numbers:\t\t");

	prev_permutation(numbers.begin(), numbers.end());
	print(numbers, "Previous permutation of numbers:\t");

	auto it = partition(numbers.begin(), numbers.end(), [](int i) {return i % 2 == 0; });

	cout << "Numbers partitioned on even and odd:\t";
	copy(numbers.begin(), it, ostream_iterator<int>(cout, " "));
	cout << "| ";
	copy(it, numbers.end(), ostream_iterator<int>(cout, " "));

	auto p = partition_point(numbers.begin(), numbers.end(), [](int i) {return i % 2 == 0; });

	cout << "\nPartitioning with partition_point\t";
	copy(numbers.begin(), p, ostream_iterator<int>(cout, " "));
	cout << "| ";
	copy(p, numbers.end(), ostream_iterator<int>(cout, " "));
	cout << '\n';
}

//count, accumulate, partial_sum, inner_product, adjacent_difference, all_of, any_of, none_of
void queries()
{
	int c = count(numbers.begin(), numbers.end(), 5);
	cout << "Count: Number 5 appears " << c << " times.\n";

	int sum = accumulate(numbers.begin(), numbers.end(), 0);
	cout << "Sum of all numbers: " << sum << '\n';

	cout << "Partial sums of numbers:\n";
	partial_sum(numbers.begin(), numbers.end(), ostream_iterator<int>(cout, " "));
	
	int product = inner_product(numbers.begin(), numbers.end(), numbers.begin(), 0);
	cout << "\nInner product of numbers: " << product << '\n';

	cout << "Adjacent difference:\n";
	adjacent_difference(numbers.begin(), numbers.end(), ostream_iterator<int>(cout, " "));

	functions(all, "\nAre all the elements odd numbers? ");
	functions(any, "\nAre there any odd numbers? ");
	functions(none, "\nAre none of the elements odd numbers? ");
	cout << '\n';
}

void functions(func f, const string &text)
{
	cout << text;
	switch (f)
	{
	case all:
		all_of(numbers.begin(), numbers.end(), [](int i) {return i % 2; })? cout << "True": cout << "False";
		break;
	case any:
		any_of(numbers.begin(), numbers.end(), [](int i) {return i % 2; })? cout << "True" : cout << "False";
		break;
	case none:
		none_of(numbers.begin(), numbers.end(), [](int i) {return i % 2; })? cout << "True" : cout << "False";
		break;
	case eq:
		equal(numbers.begin(), numbers.end(), v.begin(), v.end())? cout << "True" : cout << "False";
		break;
	case perm:
		is_permutation(numbers.begin(), numbers.end(), v.begin(), v.end())? cout << "True" : cout << "False";
		break;
	case lex:
		lexicographical_compare(numbers.begin(), numbers.end(), v.begin(), v.end())? cout << "True" : cout << "False";
		break;
	case binary:
		binary_search(numbers.begin(), numbers.end(), 23) ? cout << "True" : cout << "False";
		break;
	default:
		break;
	}
}

//equal, is_permutation, lexicographical_compare, mismatch
void comparing()
{
	for (int i = 0; i < 30; ++i)
	{
		v.push_back(rand() % 30 + 1);
	}
	print(numbers, "First collection:\t");
	print(v, "Second collection:\t");

	functions(eq, "Are the two collections equal? ");
	functions(perm, "\nAre the two collections permutations? ");
	functions(lex, "\nIs first collection lexicographicly less than the second one? ");

	auto iters = mismatch(numbers.begin(), numbers.end(), v.begin());
	cout << "\nFirst mismatching elements: " << *iters.first << " and " << *iters.second << '\n';
}

void searching()
{
	search_a_value();
	search_a_range();
	search_relative_value();
}

//find, adjacent_find, equal_range, lower_bound, upper_bound, binary_search
void search_a_value()
{
	auto it = find(numbers.begin(), numbers.end(), 5);
	it != numbers.end() ? cout << "Element " << *it << " is found at position " << it - numbers.begin() <<".\n" : cout << "Element not found in numbers.\n";

	it = adjacent_find(numbers.begin(), numbers.end());

	if (it != numbers.end())
		cout << "The first pair of repeated elements are: " << *it << " at position " << it - numbers.begin() << ".\n";

	sort(numbers.begin(), numbers.end());
	auto range = equal_range(numbers.begin(), numbers.end(), 23);
	print(numbers, "Sorted numbers:\t");
	cout << "The range of equal elements with value 23 starts after position " << (range.first - numbers.begin());
	cout << " and ends at " << (range.second - numbers.begin()) << ".\n";

	auto low = lower_bound(numbers.begin(), numbers.end(), 23);
	auto up = upper_bound(numbers.begin(), numbers.end(), 23);
	cout << "lower_bound for number 23 is at position " << (low - numbers.begin()) << ".\n";
	cout << "upper_bound for number 23 is at position " << (up - numbers.begin()) << ".\n";

	functions(binary, "Is the element 23 found with binary_search? ");
	cout << '\n';
}

//search, find_end, find_first_of
void search_a_range()
{
	int range[] = { 19, 22, 22, 23, 23 };
	cout << "The chosen range is: ";
	for (int a : range)
		cout << a << " ";
	auto it = search(numbers.begin(), numbers.end(), range, range + 4);
	it != numbers.end() ? cout << "\nRange found at position " << (it - numbers.begin()) << '\n' : cout << "\nRange not found\n";

	it = find_end(numbers.begin(), numbers.end(), range, range + 3);
	if (it != numbers.end())
		cout << "Range last found at position " << (it - numbers.begin()) << '\n';

	it = find_first_of(numbers.begin(), numbers.end(), range, range + 2);
	if (it != numbers.end())
		cout << "The first match is at position: " << *it << '\n';
}

//min_element, max_element, minmax_element
void search_relative_value()
{
	shuffle_numbers();
	print(numbers, "Shuffled numbers: ");
	cout << "The smallest element is " << *min_element(numbers.begin(), numbers.end()) << '\n';
	cout << "The largest element is " << *max_element(numbers.begin(), numbers.end()) << '\n';

	auto res = minmax_element(numbers.begin(), numbers.end());
	cout << "From minmax_element algorithm the min is " << *res.first << " and the max is " << *res.second << ".\n";
}

//copy, set_difference, set_intersection, set_union, set_symmetric_difference, merge, includes
void set_algorithms()
{
	copy(numbers.begin(), numbers.end(), inserter(s1, s1.end()));
	copy(v.begin(), v.end(), inserter(s2, s2.end()));
	print(s1, "First set:\t");
	print(s2, "Second set:\t");

	run_algorithm(sdifference, "difference");
	run_algorithm(sintersection, "intersection");
	run_algorithm(sunion, "union");
	run_algorithm(ssdifference, "symmetric difference");
	run_algorithm(smerge, "merged collection");

	if (includes(s1.begin(), s1.end(), s2.begin(), s2.end()))
		cout << "\nFirst set includes the second set.\n";
	else
		cout << "\nFirst set doesn't include the second set.\n";
}

void run_algorithm(set_algorithm alg, const string& text)
{
	vector<int> vect(60);
	std::vector<int>::iterator it;
	cout << "\nThe " << text << " has ";
	switch (alg)
	{
	case sdifference:
		it = set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), vect.begin());
		break;
	case sintersection:
		it = set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), vect.begin());
		break;
	case sunion:
		it = set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), vect.begin());
		break;
	case ssdifference:
		it = set_symmetric_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), vect.begin());
		break;
	default:
		it = merge(s1.begin(), s1.end(), s2.begin(), s2.end(), vect.begin());
		break;
	}
	vect.resize(it - vect.begin());
	cout << (vect.size()) << " elements:\n";
	for (it = vect.begin(); it != vect.end(); ++it)
		cout << ' ' << *it;
	cout << '\n';
}

//copy, move, swap_ranges, copy_backward, move_backward
void movers()
{
	vector<int> vect(numbers.size());
	copy(numbers.begin(), numbers.end(), vect.begin());
	print(vect, "Numbers copied from the first vector to a new vector:\n");

	move(v.begin(), v.end(), vect.begin());
	print(vect, "\nNumbers moved from the second vector to a new vector:\n");

	swap_ranges(numbers.begin(), numbers.end(), v.begin());
	print(numbers, "\nSwapped ranges in first and second vector:\n");
	print(v, "");

	list<int> l(numbers.size());
	copy_backward(numbers.begin(), numbers.end(), l.end());
	print(l, "\nFirst vector copied backwards: ");

	move_backward(v.begin(), v.end(), l.end());
	print(l, "Second vector moved backwards: ");
}

//fill, generate, iota, replace
void value_modifiers()
{
	fill(v.begin(), v.begin() + 7, 32);
	print(v, "Filled first 7 places of second vector with 32:\n");

	generate(v.begin(), v.begin() + 5, []() {static int i = 0; return ++i; });
	print(v, "\nFirst five elements of second vector generated from 1 to 5:\n");

	iota(v.begin(), v.begin() + 10, 21);
	print(v, "\nEncreased first 10 elements of vector starting from number 21 with algorithm iota:\n");

	replace(v.begin(), v.end(), 22, 32);
	print(v, "\nElements 22 replaced with 32:");
}

//unique, remove
void structure_changers()
{
	v.erase(unique(v.begin(), v.end()), v.end());
	print(v, "Erased elements that are not unique:\n");

	v.erase(remove(v.begin(), v.end(), 32), v.end());
	print(v, "\nRemoved and erased elements with value 32:\n");
}

//rotate_copy, reverse_copy, count_if, copy_if, fill_n, transform
void others()
{
	vector<int> vect(numbers.size());
	rotate_copy(numbers.begin(), numbers.begin() + 5, numbers.end(), vect.begin());
	print(numbers, "Rotate by 5 and copy from numbers:\n");
	print(vect, "to a new vector:\n");

	reverse_copy(numbers.begin(), numbers.end(), vect.begin());
	print(vect, "\nReversed and copied numbers to vector:\n");

	int count = count_if(numbers.begin(), numbers.end(), [](const int& n) {return n > 15; });
	cout << "\nThere are " << count << " numbers greater than 15 in the first vector.\n";

	auto it = copy_if(numbers.begin(), numbers.end(), vect.begin(), [](const int& n) {return n < 15; });
	vect.resize(it - vect.begin());
	print(vect, "\nCopied elements from first vector that are less than 15: ");

	fill_n(v.begin(), 5, 5);
	print(v, "\nFilled first 5 elements of second vector with 5:\n");

	vect.resize(numbers.size());
	transform(numbers.begin(), numbers.end(), vect.begin(), [](const int& n) {return n + 1; });
	print(numbers, "\nNumbers:\n");
	print(vect, "transformed so that every element is enlarged by 1:\n");
}