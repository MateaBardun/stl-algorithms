#pragma once

using namespace std;

enum func
{
	all,
	any,
	none,
	eq,
	perm,
	lex,
	binary
};

enum set_algorithm
{
	sdifference,
	sintersection,
	sunion,
	ssdifference,
	smerge
};

vector<int> numbers;
vector<int> v;
set<int> s1;
set<int> s2;


void create_numbers();
void shuffle_numbers();
template<typename Container>
void print(const Container& numbers, const string &text);
void print_component(string text);
void heap_operations();
void sort_operations();
void partitioning_operations();
void queries();
void functions(func f, const string &text);
void comparing();
void searching();
void search_a_value();
void search_a_range();
void search_relative_value();
void set_algorithms();
void run_algorithm(set_algorithm alg, const string &text);
void movers();
void value_modifiers();
void structure_changers();
void others();